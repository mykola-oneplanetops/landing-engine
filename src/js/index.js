import EventEmitter from "./helpers/EventEmitter";
import { isMobileDevice } from "./helpers";
import Router from "./modules/Router";
import Mask from "./modules/Mask";
import Validation from "./modules/Validation";
import dom from "./helpers/dom";

class Landing extends EventEmitter {
	constructor({ url = "/assets/flow.json" } = {}) {
		super();
		this.url = url;
	}

	init = async () => {
		this.device = isMobileDevice() ? "mobile" : "desktop";
		const flows = await this.loadFlows();
		this.flow = flows.flows.find(flow => flow.device === this.device);
		this.data = flows.data;

		const { routes } = this.flow;
		this.router = new Router(routes);
		this.router.init();

		// Testing
		dom.delegate(document.body, "select", "click", e => {
			console.log("delegation", e);
		});

		const question = this.data.contactQuestions[8];
		const createdElem = dom.create(`
			<select name="${question.id}"> 
				${question.answers
					.map(
						answer => `
							<option value="${answer.value}">
								${answer.label}
							</option>`,
					)
					.join("")}
			</select>
		`);

		this.router.active.elem.append(createdElem);

		new Mask().init();
		new Validation().init();
	};

	loadFlows = async () => {
		const res = await fetch(this.url, {
			method: "GET",
			headers: {
				"X-Requested-With": "XMLHttpRequest",
				"Content-Type": "application/json",
			},
		});

		const data = await res.json();

		return data;
	};
}

const lp = new Landing();
lp.init();
window.lp = lp;
