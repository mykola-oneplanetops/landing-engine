export const getCookie = cname => {
	const name = `${cname}=`;
	const decodedCookie = decodeURIComponent(document.cookie);
	const ca = decodedCookie.split(";");

	for (let i = 0; i < ca.length; i++) {
		let c = ca[i];
		while (c.charAt(0) == " ") {
			c = c.substring(1);
		}
		if (c.indexOf(name) === 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
};

export class URL {
	static getUrlVars() {
		const vars = {};
		const parts = window.location.href.replace(
			/[?&]+([^=&]+)=([^&]*)/gi,
			function(m, key, value) {
				vars[key] = value;
			},
		);
		return vars;
	}

	static getUrlParam(parameter, defaultvalue) {
		let urlparameter = defaultvalue;
		if (window.location.href.indexOf(parameter) > -1) {
			urlparameter = URL.getUrlVars()[parameter];
		}
		console.log(`getURLParam found '${urlparameter}' for ${parameter}`);
		return urlparameter;
	}

	static removeUrlParam(parameter) {
		const vars = URL.getUrlVars();
		delete vars[parameter];
		window.history.pushState(
			{},
			document.title,
			`/?${URL.stringifyUrlParams(vars)}`,
		);
	}

	static buildUrlParams() {
		return URL.stringifyUrlParams(URL.getUrlVars());
	}

	static stringifyUrlParams(vars) {
		const keys = Object.keys(vars);
		let s = "";
		keys.forEach(key => {
			s += `${key}=${vars[key]}`;
		});
		return s;
	}

	static getParam(parameter) {
		// first check if param is a URL param
		let val = URL.getUrlParam(parameter, "");
		console.log(`UrlParam value for ${parameter} is '${val}'`);
		if (!val == "") return val;
		// Then we look in the local store
		val = Storage.getValue(parameter);
		console.log(`Local storage value for ${parameter} is '${val}'`);
		if (typeof val !== "undefined") return val;
		return "";
	}
}

export class Storage {
	// can extend to either use browser or serverside store
	static setValue(key, val) {
		window.localStorage.setItem(key, val);
	}

	// can extend to either use browser or serverside store
	static getValue(key, defaultValue = "") {
		if (typeof defaultValue === "undefined") defaultValue = "";
		const val = window.localStorage.getItem(key);
		return val || defaultValue;
	}

	static setKeyValue(parameter, key, val) {
		const param = Storage.getValue(parameter);
		let p;
		param != "" ? (p = JSON.parse(param)) : (p = JSON.parse("{}"));
		p[key] = val;
		Storage.setValue(parameter, JSON.stringify(p));
	}

	static getKeyValue(parameter, key, defaultValue = "") {
		const val = Storage.getValue(parameter);
		if (val != "") {
			const p = JSON.parse(Storage.getValue(parameter));
			const res = p[key];
			console.log(`Storage.getKeyValue res '${res}'`);
			if (typeof res === "undefined") return defaultValue;
		}
		return defaultValue;
	}

	static setArrayValue(name, value) {
		let arr = Storage.getArrayValue(name);
		arr = arr || JSON.parse("[]");
		console.log(name, arr);
		arr.indexOf(value) === -1
			? arr.push(value)
			: arr.splice(arr.indexOf(value), 1);
		Storage.setValue(name, JSON.stringify(arr));
	}

	static getArrayValue(parameter) {
		const param = Storage.getValue(parameter);
		return param ? JSON.parse(Storage.getValue(parameter)) : false;
	}
	// static setArrayValue (parameter, arr) {
	//     var param = Storage.getValue(parameter);
	//     console.log(param);
	//     var p = param != "" ? JSON.parse(param) : JSON.parse("[]");
	//     if (p.indexOf(arr) === -1) {
	//         p.push(arr);
	//     } else {
	//         for (var i = 0; i < p.length; i++) {
	//             if (p[i] === arr) {
	//                 p.splice(i, 1);
	//             }
	//         }
	//     }
	//     Storage.setValue(parameter, JSON.stringify(p));
	// }
}

export const isMobileDevice = () => {
	return (
		typeof window.orientation !== "undefined" ||
		navigator.userAgent.indexOf("IEMobile") !== -1
	);
};
