class EventEmitter {
	constructor() {
		this._events = {};
	}

	on(type, listener) {
		this._events[type] = this._events[type] || [];
		this._events[type].push(listener);
	}

	emit(type, arg) {
		if (this._events[type]) {
			this._events[type].forEach(listener => listener(arg));
		}
	}
}

export default EventEmitter;
