/**
 * Alias for query selector
 *
 * @param {string} selector
 * @param {bool} one
 * @param  {...any} rest
 */

const dom = (selector, one, ...rest) => {
	if (one) return document.querySelector(selector, ...rest);
	return document.querySelectorAll(selector, ...rest);
};

/**
 * Alias for addEventListener
 *
 * @param {HTMLCollection || HTMLElement} elems - one or collection of elements for handler
 * @param {string} type - type of event, 'click', 'change', etc.
 * @param {function} handler - callback
 * @param  {...any} rest - props to be passed to addEventListener
 */

dom.on = (elems, type, handler, ...rest) => {
	// One element
	if (elems && !elems.length) {
		elems.addEventListener(type, handler, ...rest);
	}

	elems.forEach(elem => {
		elem.addEventListener(type, handler, ...rest);
	});
};

/**
 * Alias for removeEventListener
 *
 * Same as for addEventListener
 */
dom.off = (elems, type, handler, ...rest) => {
	// One element
	if (elems && !elems.length) {
		elems.removeEventListener(type, handler, ...rest);
	}

	elems.forEach(elem => {
		elem.removeEventListener(type, handler, ...rest);
	});
};

/**
 * Function to delegate event from parent element
 *
 * @param {HTMLElement} parent - one element to which handler will be attached
 * @param {string} selector - selector for child element, for whom we delegate event
 * @param {string} type - type of event, 'click', 'change', etc.
 * @param {function} handler - callback
 * @param  {...any} rest - props to be passed to addEventListener
 */

dom.delegate = (parent, selector, type, handler, ...rest) => {
	parent.addEventListener(
		type,
		event => {
			let { target } = event;
			let i = 1;

			while (!target.matches(selector) && i <= 5) {
				target = target.parentNode;
				// If target is <html> tag
				if (target === document.documentElement) return;
				i++;
			}

			if (target.matches(selector)) {
				handler(event);
			}
		},
		...rest,
	);
};

/**
 * Function to simply create DOM elements from the string
 *
 * @param {string} str - string which will be converted to elements
 */

dom.create = str => {
	const template = document.createElement("template");
	template.innerHTML = str;
	return template.content;
};

export default dom;
