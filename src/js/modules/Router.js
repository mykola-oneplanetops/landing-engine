import EventEmitter from "../helpers/EventEmitter";

class Router extends EventEmitter {
	active = {
		path: window.location.hash.substr(1),
		index: 0,
		elem: null,
	};

	routes = [];

	allRoutesElems = [];

	constructor(routes) {
		super();

		this.routes = [...routes];

		const routesPaths = routes.map(route => {
			return route.path;
		});
		const allRoutesSelector = `#${routesPaths.join(", #")}`;
		this.allRoutesElems = document.querySelectorAll(allRoutesSelector);
		this.allRoutesElems.forEach(routeElem => {
			// eslint-disable-next-line no-param-reassign
			routeElem.hidden = false;
		});
	}

	init = () => {
		window.addEventListener("hashchange", () => {
			this._handleChange();
		});

		this._handleChange();
	};

	_handleChange = () => {
		console.log("change");
		let neededRoute = null;

		if (!window.location.hash.length > 0) {
			neededRoute = this.routes.find(route => route.default);
			// eslint-disable-next-line prefer-destructuring
			if (!neededRoute) neededRoute = this.routes[0];
		} else {
			neededRoute = this.routes.find(route => this.isActiveRoute(route.path));
		}

		this.emit("change", neededRoute);

		this._goToRoute(neededRoute);

		this.emit("changed", neededRoute);
	};

	_setActiveRoute = (route, routeElem) => {
		this.active = {
			path: route.path,
			index: this.routes.indexOf(route),
			elem: routeElem,
		};
	};

	_goToRoute = route => {
		if (route.verifier && !route.verifier()) {
			return;
		}

		this.allRoutesElems.forEach(routeElem => {
			// eslint-disable-next-line no-param-reassign
			routeElem.hidden = true;
		});

		const routeElem = document.querySelector(`#${route.path}`);
		routeElem.hidden = false;

		this._setActiveRoute(route, routeElem);

		window.location.hash = route.path;

		this.emit("goneTo", route);
	};

	addAccess = (path, verifier) => {
		const neededRoute = this.routes.find(route => route.path === path);

		neededRoute.verifier = verifier;
	};

	isActiveRoute = path => {
		return path === window.location.hash.substr(1);
	};

	go = path => {
		const neededRoute = this.routes.find(route => route.path === path);
		this._goToRoute(neededRoute);
	};

	forward = () => {
		const nextIndex = this.active.index + 1;
		if (nextIndex > this.routes.length - 1) return;

		const nextRoute = this.routes[nextIndex];
		this._goToRoute(nextRoute);
	};

	backward = () => {
		const prevIndex = this.active.index - 1;
		if (prevIndex < 0) return;

		const prevRoute = this.routes[prevIndex];
		this._goToRoute(prevRoute);
	};
}

export default Router;
