import EventEmitter from "../helpers/EventEmitter";
import dom from "../helpers/dom";

const predefinedValidators = {
	required: {
		validator: input => {
			const { value } = input;

			return !!value;
		},
		message: "This field is required.",
	},

	name: {
		validator: input => {
			const { value } = input;
			return value.length > 1;
		},
		message: "Please provide a real name.",
	},

	zip: {
		validator: input => {
			const { value } = input;

			const reg = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
			return reg.test(value);
		},
		message: "Incorrect zip.",
	},

	email: {
		validator: input => {
			const { value } = input;

			const reg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return reg.test(value.toLowerCase());
		},
		message: "Email is not valid.",
	},

	phone: {
		validator: input => {
			const { value } = input;

			const reg = /(\d{0,3})(\d{0,3})(\d{0,4})/;
			return reg.test(value);
		},
		message: "Email is not valid.",
	},
};

class Validation extends EventEmitter {
	validators = predefinedValidators;

	constructor({ selector = "[data-validation]", autoValidate = true } = {}) {
		super();
		this.selector = selector;
		this._autoValidate = autoValidate;
	}

	init() {
		if (this._autoValidate) this.autoValidate();
	}

	autoValidate() {
		dom.delegate(document.body, this.selector, "input", event => {
			const { target } = event;

			const errors = this.validate(target);

			if (errors) {
				console.log("errors", errors);
			}
		});
	}

	validate = input => {
		let errors = null;

		const { validation } = input.dataset;
		const validator = this.validators[validation];

		if (!validator) {
			throw new Error(`Validator ${validation} doesnt exist.`);
		}

		const valid = validator.validator(input);

		if (!valid) {
			errors = {
				[validation]: validator.message,
			};
		}

		if (input.required) {
			const hasValue = this.validators.required.validator(input);

			if (!hasValue) {
				errors = {
					...errors,
					required: this.validators.required.message,
				};
			}
		}

		this.emit("validated", errors, input);
		return errors;
	};

	addValidator(key, handler) {
		this.validators[key] = handler;
	}

	removeValidator(key) {
		delete this.validators[key];
	}
}

export default Validation;
