/* eslint no-param-reassign: "off" */
import dom from "../helpers/dom";

const predefinedPatterns = {
	phone: input => {
		const x = input.value
			.replace(/\D/g, "")
			.match(/(\d{0,3})(\d{0,3})(\d{0,4})/);

		if (!x.input) {
			input.value = x.input;
		} else if (!x[2]) {
			input.value = `(${x[1]}`;
		} else if (x[2]) {
			input.value = `(${x[1]}) ${x[2]}${x[3] ? `-${x[3]}` : ""}`;
		}
	},
	int: input => {
		input.value = input.value.replace(/\D/g, "");
	},
};

class Mask {
	patterns = predefinedPatterns;

	constructor({ selector = "[data-mask]", autoBind = true } = {}) {
		this.selector = selector;
		this._autoBind = autoBind;
	}

	init() {
		if (this._autoBind) this.autoBind();
	}

	autoBind() {
		dom.delegate(document.body, this.selector, "input", event => {
			const { target } = event;

			this.bind(target);
		});
	}

	bind(input) {
		const pattern = input.dataset.mask;
		const handler = this.patterns[pattern];

		if (!handler) {
			throw new Error("Mask pattern doesnt exist.");
		}

		handler(input);
	}

	addPattern(key, handler) {
		this.patterns[key] = handler;
	}

	removePattern(key) {
		delete this.patterns[key];
	}
}

export default Mask;
