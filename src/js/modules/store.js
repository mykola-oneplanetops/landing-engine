import EventEmitter from "../helpers/EventEmitter";

class Store extends EventEmitter {
	get = key => {
		return JSON.parse(localStorage.getItem(key));
	};

	set = (key, value) => {
		this.emit(`set:${key}`);
		return JSON.stringify(localStorage.setItem(key, value));
	};
}

const store = new Store();

export default store;
